package gooddesigngumball;

public class HasQuarterState implements State {
	GumballMachine gumballMachine;
	boolean selected = false;

	public HasQuarterState(GumballMachine gumballMachine) {
		this.gumballMachine = gumballMachine;
	}

	public void insertQuarter() {
		System.out.println("You can't insert another quater");
	}

	public void ejectQuarter() {
		System.out.println("Quater returned");
		gumballMachine.setState(gumballMachine.getNoQuarterState());
	}

	public void turnCrank() {
		if (selected) {
			System.out.println("You turn...");
			gumballMachine.setState(gumballMachine.getSoldState());
			selected = false;
		} else {
			System.out.println("You have to choose the flavor first!");
		}

	}

	public void dispense() {
		System.out.println("No gumball dispensed");
//		if (selected) {
//			System.out.println();
//		} else {
//			
//		}

	}

	public void choose(String string) {
		System.out.println("You have chosen the flavor " + string);
		selected = true;
		gumballMachine.setState(gumballMachine.getHasQuarterState());
	}

	public String toString() {
		return "Machine is waiting for you to choose the flavor";
	}
}
