package gooddesigngumball;

public class GumballMachine {

	State soldOutState;
	State noQuarterState;
	State hasQuarterState;
	State soldState;
	State chooseFlavorState;
	
	State state = soldOutState;
	int count = 0;
	String string;
	
	public GumballMachine(int numberGumballs) {
		soldOutState = new SoldOutState(this);
		noQuarterState = new NoQuarterState(this);
		hasQuarterState = new HasQuarterState(this);
		soldState = new SoldState(this);
		this.count = numberGumballs;
		if (numberGumballs > 0) {
			state = noQuarterState;
		}
	}
	
	public void insertQuarter() {
		state.insertQuarter();
	}
	
	public void ejectQuarter() {
		state.ejectQuarter();
	}
	
	public void turnCrank() {
		state.turnCrank();
		state.dispense();
	}
	
	public void choose(String string){
		this.string = string;
		state.choose(string);
	}
	
	void setState(State state) {
		this.state = state;
	}
	
	void releaseBall() {
		System.out.println("A "+string+" gumball comes rolling out the slot");
		if (count != 0) {
			count = count - 1;
		}
	}

	// ------------ getter methods --------------//
	public int getCount() {
		return count;
	}
	public State getHasQuarterState() {
		return hasQuarterState;
	}
	
	public State getSoldOutState() {
		return soldOutState;
	}
	
	public State getNoQuarterState() {
		return noQuarterState;
	}
	
	public State getSoldState() {
		return soldState;
	}
	
	public String toString() {
		String header =  
			"Mighty Gumball, Inc.\n" +
			"Java-enabled Standing Gumball Model #2004\n" +
			"Inventory: " + count + " gumballs\n";
			
		return "\n" + header + state + "\n";

	}
}
