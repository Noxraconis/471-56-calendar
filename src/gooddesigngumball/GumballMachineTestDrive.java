package gooddesigngumball;

public class GumballMachineTestDrive {
	public static void main(String[] args) {
		GumballMachine gumballMachine = new GumballMachine(5);
		
		// print out the state of the machine
		System.out.println(gumballMachine);
		
		// we should get our gumball
		gumballMachine.insertQuarter();
		gumballMachine.choose("Mango");
		gumballMachine.choose("Orange");
		gumballMachine.turnCrank();
		
		System.out.println(gumballMachine);
		
		gumballMachine.insertQuarter();
		gumballMachine.ejectQuarter();
		gumballMachine.choose("Mango");
		gumballMachine.turnCrank();
		gumballMachine.insertQuarter();
		gumballMachine.choose("Orange");
		gumballMachine.turnCrank();
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		
		System.out.println(gumballMachine);

	}
}
